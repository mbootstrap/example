Mini Bootsrap Examle
====================

A first example for [mbootstrap](https://gitlab.com/mbootstrap/mbootstrap). This
example contains a mbootstrap configuration file retrieving two git repositories
and one tar archive. The repositories contain data files and a python script for
processing the data files.

~~~shell
pip install mbootstrap
mbootstrap

python ./find-script/find.py data/git/cheese Etivaz
python ./find-script/find.py data/tar/airport JFK
~~~

